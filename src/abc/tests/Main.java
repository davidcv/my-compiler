package abc.tests;

import parser.ParseException;
import parser.Parser;
import semantics.EvalVisitorState;
import semantics.UnparseVisitor;
import ast.ICalcState;

public class Main {
	public static void main(String args[]) throws ParseException {
		@SuppressWarnings("unused")
		Parser parser = new Parser(System.in);
		while (true) {
			System.out.print("> ");
			try {
				Parser.enable_tracing();
				ICalcState prog = Parser.main();
				System.out.println("Ok:  "
						+ prog.accept(new UnparseVisitor(), null));
				prog.accept(new EvalVisitorState(), null);
			} catch (Error e) {
				System.out.println("Parsing error");
				System.out.println(e.getMessage());
				e.printStackTrace();
				break;
			} catch (Exception e) {
				System.out.println("NOK.");
				e.printStackTrace();
				break;
			}
		}
	}
}
