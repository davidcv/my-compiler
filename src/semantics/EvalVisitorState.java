package semantics;

import ast.*;
import ast.com.*;
import ast.expr.*;
import ast.type.*;

public class EvalVisitorState implements IVisitor<IValue> {

	@Override
	public IValue visit(IASTStatement node, IEnv env) {
		if (node instanceof ASTDecl) {
			visit((ASTDecl) node, env);
			return new BoolValue(true);
		} else if (node instanceof ASTAssign) {
			visit((ASTAssign) node, env);
			return new BoolValue(true);
		} else if (node instanceof ASTIf) {
			visit((ASTIf) node, env);
			return new BoolValue(true);
		} else if (node instanceof ASTWhile) {
			visit((ASTWhile) node, env);
			return new BoolValue(true);
		} else if (node instanceof ASTSeq) {
			visit((ASTSeq) node, env);
			return new BoolValue(true);
		} else if (node instanceof ASTPrint) {
			visit((ASTPrint) node, env);
			return new BoolValue(true);
		} else if (node instanceof ASTPrintln) {
			visit((ASTPrintln) node, env);
			return new BoolValue(true);
		}

		return null;
	}

	private void visit(ASTDecl decl, IEnv env) {
		if (env == null)
			env = new Env();
		for (IASTExpression n : decl.eqs) {
			if (n instanceof ASTEq)
				env.assoc(((ASTEq) n).id, ((ASTEq) n).v.accept(this, env));
			else
				((ASTAssign) n).e.accept(this, env).set(
						((ASTEq) n).v.accept(this, env));
		}

		decl.com.accept(this, env.beginScope());
	}

	private void visit(ASTAssign assign, IEnv env) {
		assign.e.accept(this, env).set(assign.v.accept(this, env));
	}

	private void visit(ASTWhile whil, IEnv env) {
		Boolean cond = (Boolean) whil.cond.accept(this, env).getValue();
		while (cond) {
			whil.body.accept(this, env);
			cond = (Boolean) whil.cond.accept(this, env).getValue();
		}
	}

	private void visit(ASTIf ifE, IEnv env) {
		Boolean cond = (Boolean) ifE.cond.accept(this, env).getValue();
		if (cond) {
			ifE.ifBody.accept(this, env);
		} else {
			ifE.elseBody.accept(this, env);
		}
	}

	private void visit(ASTSeq seq, IEnv env) {
		seq.a.accept(this, env);
		seq.b.accept(this, env);
	}

	private void visit(ASTPrint print, IEnv env) {
		System.out.print(print.e.accept(this, env));
	}

	private void visit(ASTPrintln println, IEnv env) {
		System.out.println();
	}

	@Override
	public IValue visit(IASTExpression node, IEnv env) {

		if (node instanceof ASTNum) {
			return visit((ASTNum) node);
		} else if (node instanceof ASTAdd) {
			return visit((ASTAdd) node, env);
		} else if (node instanceof ASTSub) {
			return visit((ASTSub) node, env);
		} else if (node instanceof ASTMult) {
			return visit((ASTMult) node, env);
		} else if (node instanceof ASTDiv) {
			return visit((ASTDiv) node, env);
		} else if (node instanceof ASTId) {
			return visit((ASTId) node, env);
		} else if (node instanceof ASTNot) {
			return visit((ASTNot) node, env);
		} else if (node instanceof ASTDeref) {
			return visit((ASTDeref) node, env);
		} else if (node instanceof ASTGt) {
			return visit((ASTGt) node, env);
		} else if (node instanceof ASTVar) {
			return visit((ASTVar) node, env);
		} else if (node instanceof ASTBool) {
			return visit((ASTBool) node);
		}
		if (node instanceof ASTAnd) {
			return visit((ASTAnd) node, env);
		}
		return null;
	}

	private IValue visit(ASTNum number) {
		return new IntValue(number.num);
	}

	private IValue visit(ASTBool bool) {
		return new BoolValue(bool.b);
	}

	// TODO check instance of What??
	// TODO may fail here
	private IValue visit(ASTAdd plus, IEnv env) {
		Integer n1 = (Integer) plus.l.accept(this, env).getValue();
		Integer n2 = (Integer) plus.r.accept(this, env).getValue();
		return new IntValue(n1 + n2);
	}

	private IValue visit(ASTSub sub, IEnv env) {
		Integer n1 = (Integer) sub.l.accept(this, env).getValue();
		Integer n2 = (Integer) sub.r.accept(this, env).getValue();
		return new IntValue(n1 - n2);
	}

	private IValue visit(ASTMult mult, IEnv env) {
		Integer n1 = (Integer) mult.l.accept(this, env).getValue();
		Integer n2 = (Integer) mult.r.accept(this, env).getValue();
		return new IntValue(n1 * n2);
	}

	private IValue visit(ASTDiv div, IEnv env) {
		Integer n1 = (Integer) div.l.accept(this, env).getValue();
		Integer n2 = (Integer) div.r.accept(this, env).getValue();
		return new IntValue(n1 / n2);
	}

	private IValue visit(ASTId id, IEnv env) {
		return env.find(id.id);
	}

	private IValue visit(ASTNot not, IEnv env) {
		Boolean b = (Boolean) not.e.accept(this, env).getValue();
		return new BoolValue(b);
	}

	private IValue visit(ASTDeref deref, IEnv env) {
		return (IValue) env.find(deref.id).getValue();
	}

	private IValue visit(ASTGt gt, IEnv env) {
		Integer n1 = (Integer) gt.l.accept(this, env).getValue();
		Integer n2 = (Integer) gt.r.accept(this, env).getValue();
		return new BoolValue(n1 > n2);
	}

	private IValue visit(ASTVar var, IEnv env) {
		return new RefValue(var.e.accept(this, env));
	}

	private IValue visit(ASTAnd and, IEnv env) {
		Boolean b1 = (Boolean) and.l.accept(this, env).getValue();
		Boolean b2 = (Boolean) and.r.accept(this, env).getValue();
		return new BoolValue(b1 & b2);
	}
}
