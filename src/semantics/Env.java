package semantics;

import java.util.HashMap;
import java.util.Map;

import ast.IValue;

public class Env implements IEnv {

	private IEnv parent;
	private Map<String, IValue> symbolTable;

	public Env() {
		parent = null;
		symbolTable = new HashMap<String, IValue>();
	}

	@Override
	public IEnv beginScope() {
		Env child = new Env();
		child.parent = this;
		return child;
	}

	@Override
	public IEnv endScope() {
		return this.parent;
	}

	@Override
	public void assoc(String id, IValue v) {
		symbolTable.put(id, v);
	}

	@Override
	public IValue find(String id) {
		if (symbolTable.containsKey(id))
			return symbolTable.get(id);
		else if (parent != null)
			return parent.find(id);
		return null;
	}

}
