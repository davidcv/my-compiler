package semantics;

import ast.IASTExpression;
import ast.IASTStatement;
import ast.IVisitor;
import ast.com.ASTAssign;
import ast.com.ASTDecl;
import ast.com.ASTIf;
import ast.com.ASTPrint;
import ast.com.ASTPrintln;
import ast.com.ASTSeq;
import ast.com.ASTWhile;
import ast.expr.ASTAdd;
import ast.expr.ASTAnd;
import ast.expr.ASTBool;
import ast.expr.ASTDeref;
import ast.expr.ASTDiv;
import ast.expr.ASTEq;
import ast.expr.ASTGt;
import ast.expr.ASTId;
import ast.expr.ASTMult;
import ast.expr.ASTNot;
import ast.expr.ASTNum;
import ast.expr.ASTSub;
import ast.expr.ASTVar;

public class UnparseVisitor implements IVisitor<String> {

	@Override
	public String visit(IASTStatement node, IEnv env) {
		if (node instanceof ASTDecl) {
			return visit((ASTDecl) node, env);
		} else if (node instanceof ASTAssign) {
			return visit((ASTAssign) node, env);
		} else if (node instanceof ASTIf) {
			return visit((ASTIf) node, env);
		} else if (node instanceof ASTWhile) {
			return visit((ASTWhile) node, env);
		} else if (node instanceof ASTSeq) {
			return visit((ASTSeq) node, env);
		} else if (node instanceof ASTPrint) {
			return visit((ASTPrint) node, env);
		} else if (node instanceof ASTPrintln) {
			return visit((ASTPrintln) node, env);
		}

		return null;
	}

	private String visit(ASTDecl decl, IEnv env) {
		StringBuilder sb = new StringBuilder(" Decl ");
		for (IASTExpression n : decl.eqs)
			sb.append(n.accept(this, env));
		sb.append(" in ");
		sb.append(decl.com.accept(this, env) + " end");
		return sb.toString();
	}

	private String visit(ASTAssign assign, IEnv env) {
		return assign.e.accept(this, env) + ":=" + assign.v.accept(this, env);
	}

	private String visit(ASTWhile whil, IEnv env) {
		return "while(" + whil.cond.accept(this, env) + ") { "
				+ whil.body.accept(this, env) + " } ";
	}
	
	private String visit(ASTIf ifE, IEnv env) {
		return "if(" + ifE.cond.accept(this, env) + ") { "
				+ ifE.ifBody.accept(this, env) + " } else { "
				+ ifE.elseBody.accept(this, env)+" } ";
	}
	
	private String visit(ASTSeq seq, IEnv env) {
		return seq.a.accept(this, env)+" ; "+seq.b.accept(this, env);
	}

	private String visit(ASTPrint print, IEnv env) {
		return "Print("+print.e.accept(this, env)+")";
	}
	
	private String visit(ASTPrintln println, IEnv env) {
		return "Println()";
	}

	@Override
	public String visit(IASTExpression node, IEnv env) {

		if (node instanceof ASTNum) {
			return visit((ASTNum) node);
		} else if (node instanceof ASTAdd) {
			return visit((ASTAdd) node, env);
		} else if (node instanceof ASTSub) {
			return visit((ASTSub) node, env);
		} else if (node instanceof ASTMult) {
			return visit((ASTMult) node, env);
		} else if (node instanceof ASTDiv) {
			return visit((ASTDiv) node, env);
		} else if (node instanceof ASTId) {
			return visit((ASTId) node, env);
		} else if (node instanceof ASTEq) {
			return visit((ASTEq) node, env);
		} else if (node instanceof ASTNot) {
			return visit((ASTNot) node, env);
		} else if (node instanceof ASTDeref) {
			return visit((ASTDeref) node, env);
		} else if (node instanceof ASTGt) {
			return visit((ASTGt) node, env);
		} else if (node instanceof ASTVar) {
			return visit((ASTVar) node, env);
		} else if (node instanceof ASTBool) {
			return visit((ASTBool) node);
		}
		if (node instanceof ASTAnd) {
			return visit((ASTAnd) node, env);
		}
		return null;
	}

	private String visit(ASTNum number) {
		return "" + number.num;
	}

	private String visit(ASTBool bool) {
		return "" + bool.b;
	}

	private String visit(ASTAdd plus, IEnv env) {
		return "Plus(" + plus.l.accept(this, env) + ","
				+ plus.r.accept(this, env) + ")";
	}

	private String visit(ASTSub sub, IEnv env) {
		return "Sub(" + sub.l.accept(this, env) + "," + sub.r.accept(this, env)
				+ ")";
	}

	private String visit(ASTMult mult, IEnv env) {
		return "Mult(" + mult.l.accept(this, env) + ","
				+ mult.r.accept(this, env) + ")";
	}

	private String visit(ASTDiv div, IEnv env) {
		return "Div(" + div.l.accept(this, env) + "," + div.r.accept(this, env)
				+ ")";
	}

	private String visit(ASTEq eq, IEnv env) {
		return " id(" + eq.id + ")=" + eq.v.accept(this, env);
	}

	private String visit(ASTId id, IEnv env) {
		return " id(" + id.id + ")";
	}

	private String visit(ASTNot not, IEnv env) {
		return " not(" + not.e + ")";
	}

	private String visit(ASTDeref deref, IEnv env) {
		return " !" + deref.id;
	}

	private String visit(ASTGt gt, IEnv env) {
		return "Gt(" + gt.l.accept(this, env) + "," + gt.r.accept(this, env) + " )";
	}

	private String visit(ASTVar var, IEnv env) {
		return "Var(" + var.e.accept(this, env) + ")";
	}

	private String visit(ASTAnd node, IEnv env) {
		return "And(" + node.l + "," + node.r + ")";
	}

}
