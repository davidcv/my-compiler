package semantics;

import ast.IValue;

public interface IEnv {

	IEnv beginScope();

	IEnv endScope();

	void assoc(String id, IValue v);

	IValue find(String id);
}
