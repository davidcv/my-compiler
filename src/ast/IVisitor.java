package ast;

import semantics.IEnv;

public interface IVisitor<T> {
	
	T visit(IASTStatement node,IEnv env);
	T visit(IASTExpression node,IEnv env);
}
