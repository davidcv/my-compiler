package ast.expr;

import ast.ASTBaseExpr;

public class ASTBool extends ASTBaseExpr {

	public final Boolean b;

	public ASTBool(Boolean b) {
		this.b = b;
	}
}
