package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTVar extends ASTBaseExpr {
	
	public final IASTExpression e;

	public ASTVar(IASTExpression e) {
		this.e = e;
		}

}
