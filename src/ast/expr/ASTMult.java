package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTMult  extends ASTBaseExpr {

	public final IASTExpression l, r;

	public ASTMult(IASTExpression l, IASTExpression r) {
		this.l = l;
		this.r = r;
	}


}
