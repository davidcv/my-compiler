package ast.expr;

import ast.ASTBaseExpr;

public class ASTId  extends ASTBaseExpr {

	public final String id;

	public ASTId(String id) {
		this.id = id;
	}
}
