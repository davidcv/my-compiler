package ast;

import semantics.IEnv;

public abstract  class ASTBaseSTMT implements IASTStatement {

	
	public <T> T accept(IVisitor<T> visitor, IEnv env) {
		return visitor.visit(this, env);
	}
}
