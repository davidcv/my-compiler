package ast;

import semantics.IEnv;

public abstract class ASTBaseExpr implements IASTExpression {

	
	public <T> T accept(IVisitor<T> visitor, IEnv env) {
		return visitor.visit(this, env);
	}
}
