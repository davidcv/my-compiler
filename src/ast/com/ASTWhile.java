package ast.com;

import ast.ASTBaseSTMT;
import ast.IASTExpression;
import ast.IASTStatement;

public class ASTWhile extends ASTBaseSTMT {

	public final IASTExpression cond;
	public final IASTStatement body;

	public ASTWhile(IASTExpression cond, IASTStatement body) {
		this.cond = cond;
		this.body = body;

	}

}
