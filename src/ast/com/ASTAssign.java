package ast.com;

import ast.ASTBaseSTMT;
import ast.IASTExpression;

public class ASTAssign extends ASTBaseSTMT {

	public final IASTExpression v;
	public final IASTExpression e;

	public ASTAssign(IASTExpression e, IASTExpression v) {
		this.v = v;
		this.e = e;
	}
}
