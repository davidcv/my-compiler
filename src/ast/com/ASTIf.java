package ast.com;

import ast.ASTBaseSTMT;
import ast.IASTExpression;
import ast.IASTStatement;


public class ASTIf extends ASTBaseSTMT {

	public final IASTExpression cond;
	public final IASTStatement ifBody;
	public final IASTStatement elseBody;
	
	
	public ASTIf(IASTExpression cond, IASTStatement ifBody,IASTStatement elseBody) {
		this.cond = cond;
		this.ifBody = ifBody;
		this.elseBody = elseBody;
	}

	

}
