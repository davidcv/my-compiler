package ast.com;

import ast.ASTBaseSTMT;
import ast.IASTStatement;

public class ASTSeq extends ASTBaseSTMT {
	
	public final IASTStatement b;
	public final IASTStatement a;

	public ASTSeq(IASTStatement a, IASTStatement b) {
		this.a = a;
		this.b = b;
		}

}
