package ast.com;

import ast.ASTBaseSTMT;
import ast.IASTExpression;

public class ASTPrint extends ASTBaseSTMT{
	
	public final IASTExpression e;

	public ASTPrint(IASTExpression e) {
		this.e = e;
	
	}
}
