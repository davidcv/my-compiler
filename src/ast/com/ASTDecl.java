package ast.com;

import java.util.List;

import ast.ASTBaseSTMT;
import ast.IASTExpression;
import ast.IASTStatement;

public class ASTDecl  extends ASTBaseSTMT {

	public final IASTStatement com;
	public final List<IASTExpression> eqs;
	public ASTDecl(List<IASTExpression> eqs, IASTStatement com) {
		this.eqs = eqs;
		this.com = com;
	}
}
