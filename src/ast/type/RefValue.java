package ast.type;

import ast.IValue;

public class RefValue implements IValue {
	
	private IValue v;
	
	public RefValue(IValue v) {
		this.v = v;
	
	}
	
	@Override
	public void set(IValue v) {
		this.v=v;
	}

	@Override
	public IValue getValue() {
		return v;
	}
}
