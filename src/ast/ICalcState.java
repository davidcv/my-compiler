package ast;

import semantics.IEnv;

public interface ICalcState {
	<T> T accept(IVisitor<T> visitor, IEnv env);
}
